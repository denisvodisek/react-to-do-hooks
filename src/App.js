import React, {useState} from 'react';
import './App.css'
import "bootstrap/dist/css/bootstrap.min.css";
import "shards-ui/dist/css/shards.min.css"

import Todo from './components/Todo';
import TodoForm from './components/TodoForm';

import {Container, Row, Col} from "shards-react";
import { ListGroup, ListGroupItem } from "shards-react";

function App() {
  const [todos, setTodos] = useState([
    {
      text: "My first Todo item",
      isCompleted: false
    },
    {
      text: "Basketball",
      isCompleted: false
    },
    {
      text: "Pay Credit card",
      isCompleted: false
    }
  ]);

  const addTodo = text => {
    const newTodos = [...todos, { text }];
    //updating state
    setTodos(newTodos);
  };

  const completeTodo = index => {
    const newTodos = [...todos];
    newTodos[index].isCompleted = true;
    //updating state
    setTodos(newTodos);
  };

  const removeTodo = index => {
    const newTodos = [...todos];
    //removing 
    newTodos.splice(index, 1);
    //updating state
    setTodos(newTodos);
  };

  return (
    <div className='App'>
      <Container >
          <Row>
            <Col>
            <div>
              <h1>Denis's todo list with hooks!</h1>
            </div>
            <br></br>
            <ListGroup>
            {todos.length > 0 ? (
              todos.map((todo, index) => (
                <ListGroupItem key={index} >
                  <Todo
                    index={index}
                    todo={todo}
                    completeTodo={completeTodo}
                    removeTodo={removeTodo}
                  />
                  </ListGroupItem>
              )))
              :
              <ListGroupItem>Nothing to do.</ListGroupItem>
              }
              <ListGroupItem>
                <TodoForm addTodo={addTodo} />
              </ListGroupItem>
            </ListGroup>
            </Col>
        </Row>
      </Container>
    </div>
  );
}

export default App;
