import React from 'react';
import { Button } from "shards-react";
function Todo({ todo, index, completeTodo, removeTodo }) {
  return (
    <>
      <div className="todo" style={{ textDecoration: todo.isCompleted ? "line-through" : "" }}>
        {todo.text}
      </div>
      <div className="buttons">
        <Button  theme="success" size="sm" onClick={() => completeTodo(index)}>Complete</Button>{' | '}
        <Button theme="danger" size="sm" onClick={() => removeTodo(index)}>Delete</Button>
      </div>
    </>
  );
}

export default Todo;
