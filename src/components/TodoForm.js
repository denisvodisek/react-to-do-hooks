import React, {useState} from 'react';
import { Button, InputGroup, FormInput } from "shards-react";
function TodoForm({ addTodo }) {
    const [value, setValue] = useState("");
  
    const handleSubmit = e => {
      e.preventDefault();
      if (!value) 
        return;
      addTodo(value);
      setValue("");
    };
  
    const handleChange = e => {
      setValue(e.target.value)
    }
    const handleClick = () => {
      if (!value) 
        return;
      addTodo(value);
      setValue("");
    }
    return (
      <form onSubmit={handleSubmit}>
        <InputGroup className="mb-2">
        <FormInput placeholder="Add todo"
          type="text"
          className="input"
          value={value}
          onChange={handleChange}
        />
        </InputGroup>
        <Button theme="primary" size="sm" onClick={handleClick}>Add</Button>
      </form>
    );
  }

  export default TodoForm;